"""Assignment 1: Cryptography for CS41 Winter 2020.

Name: Fulop Dezso-Samson
ID: fdim1814

"""
# from .utils import *
import string
from itertools import cycle
import random
import utils


#################
# CAESAR CIPHER #
#################

def gen_caesar_dict(offset):
    dictionary = dict()
    for i in range(26-offset):
        dictionary[string.ascii_uppercase[i]] = string.ascii_uppercase[i + offset]
    for i in range(offset):
        dictionary[string.ascii_uppercase[26 - offset + i]] = string.ascii_uppercase[i]
    return dictionary


def gen_vigenere_dict():
    dictionary = dict()
    for i in range(25):
        dictionary[string.ascii_uppercase[i]] = i
    return dictionary


def encrypt_caesar(plaintext):
    # dictionary = gen_caesar_dict(3)
    dictionary = {'A': 'D', 'B': 'E', 'C': 'F', 'D': 'G', 'E': 'H', 'F': 'I', 'G': 'J', 'H': 'K', 'I': 'L', 'J': 'M',
                  'K': 'N', 'L': 'O', 'M': 'P', 'N': 'Q', 'O': 'R', 'P': 'S', 'Q': 'T', 'R': 'U', 'S': 'V', 'T': 'W',
                  'U': 'X', 'V': 'Y', 'W': 'Z', 'X': 'A', 'Y': 'B', 'Z': 'C'}
    plaintext = plaintext.upper()
    cipher_text = ''
    for letter in plaintext:
        if letter in string.ascii_uppercase:
            cipher_text += dictionary[letter]
    return cipher_text


def decrypt_caesar(cipher_text):
    # dictionary = dict((v, k) for k, v in gen_caesar_dict(3).items())
    dictionary = {'D': 'A', 'E': 'B', 'F': 'C', 'G': 'D', 'H': 'E', 'I': 'F', 'J': 'G', 'K': 'H', 'L': 'I', 'M': 'J',
                  'N': 'K', 'O': 'L', 'P': 'M', 'Q': 'N', 'R': 'O', 'S': 'P', 'T': 'Q', 'U': 'R', 'V': 'S', 'W': 'T',
                  'X': 'U', 'Y': 'V', 'Z': 'W', 'A': 'X', 'B': 'Y', 'C': 'Z'}
    cipher_text = cipher_text.upper()
    plaintext = ''
    for letter in cipher_text:
        if letter in string.ascii_uppercase:
            plaintext += dictionary[letter]
    return plaintext


###################
# VIGENERE CIPHER #
###################

def encrypt_vigenere(plaintext, keyword):
    # dictionary = gen_vigenere_dict()
    dictionary = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7, 'I': 8, 'J': 9, 'K': 10, 'L': 11,
                  'M': 12, 'N': 13, 'O': 14, 'P': 15, 'Q': 16, 'R': 17, 'S': 18, 'T': 19, 'U': 20, 'V': 21, 'W': 22,
                  'X': 23, 'Y': 24, 'Z': 25}
    cipher_text = ''
    i = 0
    n = len(keyword)
    for letter in plaintext:
        cipher_text += string.ascii_uppercase[(dictionary[letter] + dictionary[keyword[i % n]]) % 26]
        i += 1
    return cipher_text


def decrypt_vigenere(cipher_text, keyword):
    # dictionary = gen_vigenere_dict()
    dictionary = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7, 'I': 8, 'J': 9, 'K': 10, 'L': 11,
                  'M': 12, 'N': 13, 'O': 14, 'P': 15, 'Q': 16, 'R': 17, 'S': 18, 'T': 19, 'U': 20, 'V': 21, 'W': 22,
                  'X': 23, 'Y': 24, 'Z': 25}
    plaintext = ''
    n = len(keyword)
    i = 0
    for letter in cipher_text:
        plaintext += string.ascii_uppercase[(dictionary[letter] - dictionary[keyword[i % n]]) % 26]
        i += 1
    return plaintext


##################
# SCYTALE CIPHER #
##################

def encrypt_scytale(plaintext, circumference):
    cipher_text = ''
    n = len(plaintext)
    for step in range(circumference):
        slicer = slice(step, n, circumference)
        cipher_text += plaintext[slicer]
    return cipher_text


def decrypt_scytale(cipher_text, circumference):
    plaintext = ''
    n = len(cipher_text)
    interval = n // circumference
    for step in range(interval):
        slicer = slice(step, n, interval)
        plaintext += cipher_text[slicer]
    return plaintext


####################
# RAILFENCE CIPHER #
####################

def encrypt_railfence(plaintext, num_rails):
    k = (num_rails - 1) * 2
    interval = k
    n = len(plaintext)
    cipher_text = plaintext[slice(0, n, interval)]
    for i in range(1, num_rails - 1):
        interval -= 2
        j = i
        small_step = False
        while j < n:
            cipher_text += plaintext[j]
            if small_step is True:
                j += k - interval
                small_step = False
            else:
                small_step = True
                j += interval
    cipher_text += plaintext[slice(num_rails - 1, n, k)]
    return cipher_text


def rail_pattern(n):
    r = list(range(n))
    return cycle(r + r[-2:0:-1])


def decrypt_railfence(cipher_text, num_rails):
    p = rail_pattern(num_rails)
    indexes = sorted(range(len(cipher_text)), key=lambda j: next(p))
    result = [''] * len(cipher_text)
    for i, c in zip(indexes, cipher_text):
        result[i] = c
    return ''.join(result)


########################################
# MERKLE-HELLMAN KNAPSACK CRYPTOSYSTEM #
########################################


def generate_private_key(n=8):
    """Generate a private key to use with the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key
    components of the MH Cryptosystem. This consists of 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        Note: You can double-check that a sequence is superincreasing by using:
            `utils.is_superincreasing(seq)`
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q`
        Note: You can use `utils.coprime(r, q)` for this.

    You'll also need to use the random module's `randint` function, which you
    will have to import.

    Somehow, you'll have to return all three of these values from this function!
    Can we do that in Python?!

    :param n: Bitsize of message to send (defaults to 8)
    :type n: int

    :returns: 3-tuple private key `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    raise NotImplementedError('generate_private_key is not yet implemented!')


def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in
    the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one or two lines using list comprehensions.

    :param private_key: The private key created by generate_private_key.
    :type private_key: 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    :returns: n-tuple public key
    """
    # Your implementation here.
    raise NotImplementedError('create_public_key is not yet implemented!')


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    Following the outline of the handout, you will need to:
    1. Separate the message into chunks based on the size of the public key.
        In our case, that's the fixed value n = 8, corresponding to a single
        byte. In principle, we should work for any value of n, but we'll
        assert that it's fine to operate byte-by-byte.
    2. For each byte, determine its 8 bits (the `a_i`s). You can use
        `utils.byte_to_bits(byte)`.
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted cipher_texts for each chunk of the message.

    Hint: Think about using `zip` and other tools we've discussed in class.

    :param message: The message to be encrypted.
    :type message: bytes
    :param public_key: The public key of the message's recipient.
    :type public_key: n-tuple of ints

    :returns: Encrypted message bytes represented as a list of ints.
    """
    # Your implementation here.
    raise NotImplementedError('encrypt_mh is not yet implemented!')


def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key.

    Following the outline of the handout, you will need to:
    1. Extract w, q, and r from the private key.
    2. Compute s, the modular inverse of r mod q, using the Extended Euclidean
        algorithm (implemented for you at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum problem using c' and w to recover
        the original plaintext byte.
    5. Reconstitute the decrypted bytes to form the original message.

    :param message: Encrypted message chunks.
    :type message: list of ints
    :param private_key: The private key of the recipient (you).
    :type private_key: 3-tuple of w, q, and r

    :returns: bytearray or str of decrypted characters
    """
    # Your implementation here.
    raise NotImplementedError('decrypt_mh is not yet implemented!')
