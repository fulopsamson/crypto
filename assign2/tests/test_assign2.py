import unittest
from assign2 import crypto as utils
from assign2.deck import Deck


class UtilsTest(unittest.TestCase):
    def setUp(self) -> None:
        self.private_key = utils.generate_private_key()
        self.public_key = utils.create_public_key(self.private_key)
        self.first_half = utils.generate_solitaire_half_key()
        self.second_half = utils.generate_solitaire_half_key(flag=1)
        self.common_key = utils.generate_common_key(self.first_half, self.second_half)

    def test_superincreasing_seq(self):
        self.assertTrue(utils.is_superincreasing(utils.generate_superincreasing_seq(8)))
        self.assertTrue(utils.is_superincreasing(utils.generate_superincreasing_seq(13)))
        self.assertEqual(len(utils.generate_superincreasing_seq(10)), 10)

    def test_private_key(self):
        self.assertTrue(utils.coprime(self.private_key[1], self.private_key[2]))

    def test_key_encoding_decoding(self):
        self.assertEqual(utils.decrypt_mh(utils.encrypt_mh(b'dummmymessage', self.public_key),
                                          self.private_key), b'dummmymessage')
        self.assertEqual(utils.decrypt_mh(utils.encrypt_mh(b'test_message', self.public_key), self.private_key),
                         utils.decrypt_mh(utils.encrypt_mh(b'test_message', self.public_key), self.private_key))
        self.assertEqual(len(utils.encrypt_mh(b'The quick brown fox jumps over the lazy dog', self.public_key)),
                         len('The quick brown fox jumps over the lazy dog'))

    def test_translate_and_reverse(self):
        self.assertEqual(utils.translate('abcdefghijklmnopqrstuvwxyz'), list(range(26)))
        self.assertEqual(utils.reverse(utils.translate("Thequickbrownfoxjumpsoverthelazydog")),
                         "THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG")
        self.assertEqual(len(utils.translate('THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG')),
                         len('THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG'))

    def test_solitaire_key_gen(self):
        self.assertEqual(len(self.first_half), len(self.second_half), 27)
        self.assertEqual(len(self.common_key), 54)
        self.assertIn(0, self.first_half)
        self.assertIn(53, self.second_half)
        self.assertEqual(self.common_key, self.first_half + self.second_half)
        self.assertEqual(list(range(27)), sorted(self.first_half))
        self.assertEqual(list(range(27, 54)), sorted(self.second_half))

    def test_deck_operations(self):
        deck = Deck([5, 17, 7, 14, 2, 23, 18, 0, 26, 4, 19, 13,
                     10, 16, 12, 15, 22, 20, 11, 9, 8, 6, 1, 24,
                     25, 21, 3, 34, 40, 50, 42, 53, 32, 28, 30,
                     44, 39, 41, 35, 29, 43, 45, 46, 33, 36, 31,
                     49, 48, 52, 47, 51, 27, 37, 38])
        deck.find_joker_a_and_switch()
        self.assertEqual(deck.get_cards(), [5, 17, 7, 14, 2, 23, 18, 26, 0, 4, 19, 13,
                                            10, 16, 12, 15, 22, 20, 11, 9, 8, 6, 1, 24,
                                            25, 21, 3, 34, 40, 50, 42, 53, 32, 28, 30,
                                            44, 39, 41, 35, 29, 43, 45, 46, 33, 36, 31,
                                            49, 48, 52, 47, 51, 27, 37, 38])
        deck.find_joker_b_and_switch()
        self.assertEqual(deck.get_cards(), [5, 17, 7, 14, 2, 23, 18, 26, 0, 4, 19, 13,
                                            10, 16, 12, 15, 22, 20, 11, 9, 8, 6, 1, 24,
                                            25, 21, 3, 34, 40, 50, 42, 32, 28, 53, 30,
                                            44, 39, 41, 35, 29, 43, 45, 46, 33, 36, 31,
                                            49, 48, 52, 47, 51, 27, 37, 38])
        deck.triple_cut()
        self.assertEqual(deck.get_cards(), [30, 44, 39, 41, 35, 29, 43, 45, 46, 33, 36,
                                            31, 49, 48, 52, 47, 51, 27, 37, 38, 0, 4, 19,
                                            13, 10, 16, 12, 15, 22, 20, 11, 9, 8, 6, 1, 24,
                                            25, 21, 3, 34, 40, 50, 42, 32, 28, 53, 5, 17,
                                            7, 14, 2, 23, 18, 26])
        deck.count_cut()
        self.assertEqual(deck.get_cards(), [12, 15, 22, 20, 11, 9, 8, 6, 1, 24,
                                            25, 21, 3, 34, 40, 50, 42, 32, 28, 53, 5, 17,
                                            7, 14, 2, 23, 18, 30, 44, 39, 41, 35, 29, 43, 45, 46, 33, 36,
                                            31, 49, 48, 52, 47, 51, 27, 37, 38, 0, 4, 19,
                                            13, 10, 16, 26])
        deck = Deck(self.common_key)
        deck.do_cycle()
        self.assertEqual(len(deck.get_cards()), 54)

    def test_solitaire_encryption(self):
        key1 = Deck(self.common_key)
        key2 = Deck(self.common_key)
        self.assertEqual(utils.solitaire_decrypt(utils.solitaire_encrypt("testmessage", key1), key2),
                         "TESTMESSAGE")
        self.assertEqual(len(utils.solitaire_encrypt("testmessage", key1)), len("testmessage"))


if __name__ == '__main__':
    unittest.main()
