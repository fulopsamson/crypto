import socket
import logging
import json

host, port = 'localhost', 12356


if __name__ == '__main__':
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    logging.basicConfig(level=logging.INFO)
    logging.info("Server socket bound to port %d", port)
    server_socket.listen()
    logging.info("Server started")
    while True:
        conn1, address1 = server_socket.accept()
        logging.info("Server received connection from %s", address1)
        conn2, address2 = server_socket.accept()
        logging.info("Server received connection from %s", address2)
        raw = json.loads(conn1.recv(1024))
        key1 = raw['key']
        id1 = raw['id']
        logging.info(f"Key 1: {key1}")
        conn1.send(b'0')
        raw = json.loads(conn2.recv(1024))
        key2 = raw['key']
        id2 = raw['id']
        logging.info(f"Key 2: {key2}")
        conn2.send(b'1')
        conn1.send(json.dumps({'partner_key': key2, "partner_port": id2}).encode())
        conn2.send(json.dumps({'partner_key': key1, "partner_port": id1}).encode())
        logging.info(f'Server\'s work it\'s done here, closing connection with {address1} and {address2}')
        conn1.close()
        conn2.close()
