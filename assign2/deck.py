class Deck:
    """This is a card deck for a solitaire ciphering"""

    def __init__(self, cards):
        self.cards = cards.copy()

    def get_cards(self):
        return self.cards

    def find_joker_a_and_switch(self):
        c = self.cards.index(0)
        if c == 53:
            self.cards[0], self.cards[53] = self.cards[53], self.cards[0]
        else:
            self.cards[c + 1], self.cards[c] = self.cards[c], self.cards[c + 1]

    def find_joker_b_and_switch(self):
        c = self.cards.index(53)
        if c == 53:
            self.cards[1], self.cards[0], self.cards[c] = self.cards[c], self.cards[1], self.cards[0]
        elif c == 52:
            self.cards[0], self.cards[c + 1], self.cards[c] = self.cards[c], self.cards[0], self.cards[c + 1]
        else:
            self.cards[c + 2], self.cards[c + 1], self.cards[c] = self.cards[c], self.cards[c + 2], self.cards[c + 1]

    def triple_cut(self):
        a = self.cards.index(0)
        b = self.cards.index(53)
        if a < b:
            i = a
            j = b
        else:
            i = b
            j = a
        x = self.cards[j + 1:] + self.cards[i:j + 1] + self.cards[:i]
        self.cards = x

    def count_cut(self):
        cutter = self.cards[53]
        x = self.cards[cutter:53] + self.cards[:cutter] + [self.cards[53]]
        self.cards = x

    def get_output_letter(self):
        return self.cards[self.cards[0]]

    def do_cycle(self):
        self.find_joker_a_and_switch()
        self.find_joker_b_and_switch()
        self.triple_cut()
        self.count_cut()
