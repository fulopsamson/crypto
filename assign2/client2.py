import json
import logging
import socket
import sys
import crypto

host, port = 'localhost', 12356
client_socket = socket.socket()
private_key = crypto.generate_private_key()
public_key = crypto.create_public_key(private_key)


def test_if_port_is_free(port_nr):
    a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result_of_check = a_socket.connect_ex((host, port_nr))
    if result_of_check == 0:
        a_socket.close()
        return False
    else:
        a_socket.close()
        return True


def main():
    logging.basicConfig(level=logging.INFO)
    try:
        listener_port = int(sys.argv[1])
    except IndexError:
        logging.error("Usage: python client.py [port_number]")
        return
    if test_if_port_is_free(listener_port) is False:
        logging.error("Port number already in use")
        return
    try:
        client_socket.connect((host, port))
    except ConnectionRefusedError:
        logging.error("Key server is not running")
        return
    logging.info("Connection established with the server")
    logging.info(f'public key: {public_key}')
    data = {'key': public_key, 'id': listener_port}
    client_socket.send(json.dumps(data).encode())
    num = client_socket.recv(1024).decode()
    logging.info(num)
    raw = json.loads(client_socket.recv(1024).decode())
    partner_key = raw['partner_key']
    partner_port = int(raw['partner_port'])
    logging.info(f"Got partners key: {partner_key}, port: {partner_port}")
    client_socket.close()
    if int(num) == 0:
        logging.info(f"This is the host client, opening socket on port {listener_port}")
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind((host, listener_port))
        server_socket.listen()
        logging.info(f"Client socket listening on {host, listener_port}")
        conn, address = server_socket.accept()
        logging.info(f"Got connection from {address}")
        msg = json.loads(conn.recv(1024).decode())["msg"]
        logging.info(f"Message received: {crypto.decrypt_mh(msg, private_key).decode()}")
        logging.info("Sending encrypted greeting message")
        encoded_message = crypto.encrypt_mh(b'Greetings', partner_key)
        conn.send(json.dumps({"msg": encoded_message}).encode())
        first_half = crypto.generate_solitaire_half_key(int(num))
        msg = json.loads(conn.recv(1024).decode())
        second_half = msg['half_key']
        logging.info(f"Received half solitaire key: {second_half}")
        common_key = crypto.generate_common_key(first_half, second_half)
        logging.info(f"Common key: {common_key}")
        msg = json.dumps({"half_key": first_half}).encode()
        conn.send(msg)
        raw = json.loads(conn.recv(1024).decode())
        solitaire_key = crypto.Deck(common_key.copy())
        msg = crypto.solitaire_decrypt(raw['msg'], solitaire_key)
        logging.info(f"Received message: {msg}")
        encoded_message = crypto.solitaire_encrypt("THESECONDMESSAGETHESECONDMESSAGETHESECONDMESSAGETHESECONDMESSAGETHESECONDMESSAGETHESECONDMESSAGETHESECONDMESSAGETHESECONDMESSAGE"
                                                   "THESECONDMESSAGETHESECONDMESSAGETHESECONDMESSAGETHESECONDMESSAGETHESECONDMESSAGETHESECONDMESSAGETHESECONDMESSAGETHESECONDMESSAGE", solitaire_key)
        conn.send(json.dumps({"msg": encoded_message}).encode())
        # got common key
    else:
        partner_socket = socket.socket()
        partner_socket.connect((host, partner_port))
        logging.info("Sending encrypted greeting message")
        encoded_message = crypto.encrypt_mh(b'Greetings', partner_key)
        partner_socket.send(json.dumps({"msg": encoded_message}).encode())
        msg = json.loads(partner_socket.recv(1024).decode())["msg"]
        logging.info(f"Message received: {crypto.decrypt_mh(msg, private_key).decode()}")
        second_half = crypto.generate_solitaire_half_key(int(num))
        msg = json.dumps({"half_key": second_half}).encode()
        partner_socket.send(msg)
        msg = json.loads(partner_socket.recv(1024).decode())
        first_half = msg['half_key']
        logging.info(f"Received half solitaire key: {second_half}")
        common_key = crypto.generate_common_key(first_half, second_half)
        logging.info(f"Common key: {common_key}")
        solitaire_key = crypto.Deck(common_key.copy())
        encoded_message = crypto.solitaire_encrypt("THISISTHEMESSAGETHISISTHEMESSAGETHISISTHEMESSAGETHISISTHEMESSAGETHISISTHEMESSAGETHISISTHEMESSAGETHISISTHEMESSAGETHISISTHEMESSAGE"
                                                   "THISISTHEMESSAGETHISISTHEMESSAGETHISISTHEMESSAGETHISISTHEMESSAGETHISISTHEMESSAGETHISISTHEMESSAGETHISISTHEMESSAGETHISISTHEMESSAGE", solitaire_key)
        msg = json.dumps({"msg": encoded_message})
        partner_socket.send(msg.encode())
        raw = json.loads(partner_socket.recv(1024).decode())
        msg = crypto.solitaire_decrypt(raw['msg'], solitaire_key)
        logging.info(f"Received message: {msg}")
        # got common key


if __name__ == '__main__':
    main()
