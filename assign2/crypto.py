import math
import random
import string


class Deck:
    """This is a card deck for a solitaire ciphering"""

    def __init__(self, cards):
        self.cards = cards.copy()

    def get_cards(self):
        return self.cards

    def find_joker_a_and_switch(self):
        c = self.cards.index(0)
        if c == 53:
            self.cards[0], self.cards[53] = self.cards[53], self.cards[0]
        else:
            self.cards[c + 1], self.cards[c] = self.cards[c], self.cards[c + 1]

    def find_joker_b_and_switch(self):
        c = self.cards.index(53)
        if c == 53:
            self.cards[1], self.cards[0], self.cards[c] = self.cards[c], self.cards[1], self.cards[0]
        elif c == 52:
            self.cards[0], self.cards[c + 1], self.cards[c] = self.cards[c], self.cards[0], self.cards[c + 1]
        else:
            self.cards[c + 2], self.cards[c + 1], self.cards[c] = self.cards[c], self.cards[c + 2], self.cards[c + 1]

    def triple_cut(self):
        a = self.cards.index(0)
        b = self.cards.index(53)
        if a < b:
            i = a
            j = b
        else:
            i = b
            j = a
        x = self.cards[j + 1:] + self.cards[i:j + 1] + self.cards[:i]
        self.cards = x

    def count_cut(self):
        cutter = self.cards[53]
        x = self.cards[cutter:53] + self.cards[:cutter] + [self.cards[53]]
        self.cards = x

    def get_output_letter(self):
        return self.cards[self.cards[0]]

    def do_cycle(self):
        self.find_joker_a_and_switch()
        self.find_joker_b_and_switch()
        self.triple_cut()
        self.count_cut()


class Error(Exception):
    """Base class for exceptions in this module."""


class BinaryConversionError(Error):
    """Custom exception for invalid binary conversions."""


class NotCoprimeError(Error):
    """Custom exception for arguments that are not coprime but need to be."""


class NotSupportedCharacter(Error):
    """Custom exception for messages what contains other than letters."""


def byte_to_bits(byte):
    """Convert a byte to an tuple of 8 bits for use in Merkle-Hellman.

    The first element of the returned tuple is the most significant bit.

    Usage::
        byte_to_bits(65)  # => [0, 1, 0, 0, 0, 0, 0, 1]
        byte_to_bits(b'ABC'[0])  # => [0, 1, 0, 0, 0, 0, 0, 1]
        byte_to_bits('A')  # => raises TypeError

    :param byte: The byte to convert.
    :type byte: int between 0 and 255, inclusive.

    :raises: BinaryConversionError if byte is not in [0, 255].
    :returns: An 8-tuple of bits representing this byte's value.
    """
    if not 0 <= byte <= 255:
        raise BinaryConversionError(byte)

    out = []
    for i in range(8):
        out.append(byte & 1)
        byte >>= 1
    return tuple(out[::-1])


def coprime(a, b):
    """Return whether a and b are coprime.

    Two numbers are coprime if and only if their greater common divisor is 1.

    Usage::

        print(coprime(5, 8))  # => True (5 and 8 have no common divisors)
        print(coprime(6, 9))  # => False (6 and 9 are both divisible by 3)
    """
    return math.gcd(a, b) == 1


def modinv(a, b):
    """Return the modular inverse of a mod b.

    The returned value s satisfies a * s == 1 (mod b).

    As a precondition, a should be less than b and a and b must be coprime.
    Errors are raised if these conditions do not hold.

    Adapted from https://en.wikibooks.org/wiki/Algorithm_Implementation/
    Mathematics/Extended_Euclidean_algorithm#Python

    :param a: Value whose modular inverse to find.
    :param b: The modulus.

    :raises: ValueError if a >= b.
    :raises: NotCoprimeError if a and b are not coprime.

    :returns: The modular inverse of a mod b.
    """
    if a >= b:
        raise ValueError("First argument to modinv must be less than the second argument.")
    if not coprime(a, b):
        raise NotCoprimeError("Mathematically impossible to find modular inverse of non-coprime values.")

    # Actually find the modular inverse.
    saved = b
    x, y, u, v = 0, 1, 1, 0
    while a:
        q, r = b // a, b % a
        m, n = x - u*q, y - v*q
        # Tuple packing and unpacking can be useful!
        b, a, x, y, u, v = a, r, u, v, m, n
    return x % saved


def is_superincreasing(seq):
    """Return whether a given sequence is superincreasing.

    A sequence is superincreasing if each element is greater than the sum of
    all elements before it.

    Usage::

        is_superincreasing([1, 1, 1, 1, 1])  # => False
        is_superincreasing([1, 3, 4, 9, 15, 90])  # => False
        is_superincreasing([1, 2, 4, 8, 16])  # => True

    :param seq: The iterable to check.
    :returns: Whether this sequence is superincreasing.
    """
    total = 0  # Total so far
    for n in seq:
        if n <= total:
            return False
        total += n
    return True


def generate_superincreasing_seq(n):
    """Returns a superincreasing sequence.

    A sequence is superincreasing if each element is greater than the sum of
    all elements before it.

    :param n: The length of the sequence.
    :returns: The generated sequence.
    """
    summa = 0
    super_seq = list()
    for i in range(n):
        num = random.randint(summa + 1, 2 * (summa + 1))
        super_seq.append(num)
        summa += num
    return super_seq


########################################
# MERKLE-HELLMAN KNAPSACK CRYPTOSYSTEM #
########################################


def generate_private_key(n=8):
    """Generate a private key to use with the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key
    components of the MH Cryptosystem. This consists of 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        Note: You can double-check that a sequence is superincreasing by using:
            `utils.is_superincreasing(seq)`
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q`
        Note: You can use `utils.coprime(r, q)` for this.

    :param n: Bit size of message to send (defaults to 8)
    :type n: int

    :returns: 3-tuple private key `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    w = generate_superincreasing_seq(n)
    k = sum(w)
    q = random.randint(k + 1, 2 * (k + 1))
    r = q//2
    while r >= 2 and math.gcd(r, q) != 1:
        r -= 1
    return w, q, r


def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in
    the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one or two lines using list comprehensions.

    :param private_key: The private key created by generate_private_key.
    :type private_key: 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    :returns: n-list public key
    """
    return list(map(lambda x, r0=private_key[2], q0=private_key[1]: (r0 * x) % q0, private_key[0]))


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    Following the outline of the handout, you will need to:
    1. Separate the message into chunks based on the size of the public key.
        In our case, that's the fixed value n = 8, corresponding to a single
        byte. In principle, we should work for any value of n, but we'll
        assert that it's fine to operate byte-by-byte.
    2. For each byte, determine its 8 bits (the `a_i`s). You can use
        `utils.byte_to_bits(byte)`.
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk of the message.

    Hint: Think about using `zip` and other tools we've discussed in class.

    :param message: The message to be encrypted.
    :type message: bytes
    :param public_key: The public key of the message's recipient.
    :type public_key: n-tuple of ints

    :returns: Encrypted message bytes represented as a list of ints.
    """
    cipher_text = list()
    for b in message:
        c = sum(map(lambda x: x[0] * x[1], zip(byte_to_bits(b), public_key)))
        cipher_text.append(c)
    return cipher_text


def solve_superincreasing_sum_problem(private_key, c):
    """Calculates the indexes of numbers from w witch sum is c

    :param c: The sum I have to solve
    :type c: int
    :param private_key: The private key of the recipient
    :type private_key: 3-tuple of w, q, and r
    :returns: list with the used value indexes
    """
    w = private_key[0]      # I only need the superincreasing sequence
    indexes = []
    index = len(w) - 1
    while c > 0:
        if w[index] <= c:
            indexes.append(index)
            c -= w[index]
        index -= 1
    return indexes


def build_message(indexes, n=8):
    """Return the actual number from the indexes

    :param indexes: The indexes, basically the places where to put 1
    type indexes: list of int

    :param n: the chunk size
    :type n: int

    :returns: int number
    """
    x = 0
    for i in indexes:
        x += pow(2, n - i - 1)
    return x


def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key.

    Following the outline of the handout, you will need to:
    1. Extract w, q, and r from the private key.
    2. Compute s, the modular inverse of r mod q, using the Extended Euclidean
        algorithm (implemented for you at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum problem using c' and w to recover
        the original plaintext byte.
    5. Reconstitute the decrypted bytes to form the original message.

    :param message: Encrypted message chunks.
    :type message: list of ints

    :param private_key: The private key of the recipient (you).
    :type private_key: 3-tuple of w, q, and r

    :returns: bytearray or str of decrypted characters
    """
    w, q, r = private_key
    s = modinv(r, q)
    plain_text = b''
    for i in message:
        c = i * s % q
        x = solve_superincreasing_sum_problem(private_key, c)
        plain_text += chr(build_message(x)).encode()
    return plain_text


def generate_solitaire_half_key(flag=0):
    """Generates the half of a solitaire key

    :param flag: indicates witch half of the key must be generated
    :type flag: int

    :returns array of int"""
    bottom = 1
    top = 27
    if flag == 1:
        bottom = 27
        top = 53
    deck = (list(range(bottom, top)))
    if flag == 1:
        deck.append(53)
    else:
        deck.append(0)
    random.shuffle(deck)
    return deck


def generate_common_key(first_half, second_half):
    """Combines the two clients keys to a common key
    This could be more complicated, but it is what it is :))

    :param first_half: first part of the key
    :type first_half: array of integers between the values 0 - 53

    :param second_half: second part of the key
    :type first_half: array of integers between the values 0 - 53

    :returns: array of the two key combined"""
    return first_half + second_half


def translate(message):
    """Checks if the message is correct, and translates it to integers
    :param message: the message witch will be encrypted
    :type message string

    :returns: a list of int
    """
    message = message.upper()
    for letter in message:
        if letter not in string.ascii_uppercase:
            raise NotSupportedCharacter

    dictionary = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7, 'I': 8, 'J': 9,
                  'K': 10, 'L': 11, 'M': 12, 'N': 13, 'O': 14, 'P': 15, 'Q': 16, 'R': 17, 'S': 18,
                  'T': 19, 'U': 20, 'V': 21, 'W': 22, 'X': 23, 'Y': 24, 'Z': 25}
    out = []
    for i in message:
        out.append(dictionary[i])
    return out


def reverse(values):
    """Converts a list of integers to a string

    :param values: list of integers
    :type values: integer array

    :returns a string"""
    dictionary = {0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E', 5: 'F', 6: 'G', 7: 'H', 8: 'I', 9: 'J',
                  10: 'K', 11: 'L', 12: 'M', 13: 'N', 14: 'O', 15: 'P', 16: 'Q', 17: 'R', 18: 'S',
                  19: 'T', 20: 'U', 21: 'V', 22: 'W', 23: 'X', 24: 'Y', 25: 'Z'}
    out = ''
    for i in values:
        out += (dictionary[i])
    return out


def solitaire_encrypt(message, key):
    """Encrypts a string using the solitaire card encryption method
    :param message: the text to be encrypted
    :type message: str

    :param key: the encryption key, basically a card deck order
    :type key: Deck

    :returns: the encrypted message
    """
    values = translate(message)
    output = []
    for i in values:
        key.do_cycle()
        c = key.get_output_letter()
        output.append((c + i) % 26)
    return reverse(output)


def solitaire_decrypt(message, key):
    """Decrypts a string using the solitaire card encryption method
    :param message: the text to be decrypted
    :type message: str

    :param key: the encryption key, basically a card deck order
    :type key: Deck

    :returns: the decrypted message
    """
    values = translate(message)
    output = []
    for i in values:
        key.do_cycle()
        c = key.get_output_letter()
        output.append((i - c) % 26)
    return reverse(output)
