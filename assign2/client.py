import json
import logging
import socket
import crypto
import sys
import threading

host, port = 'localhost', 12356
client_socket = socket.socket()
private_key = crypto.generate_private_key()
public_key = crypto.create_public_key(private_key)
ALWAYS_ACCEPT_CONNECTION = True
listener_port = 16666


def login():
    data = {'command': 'login', 'data': {'key': public_key, 'id': listener_port}}
    client_socket.send(json.dumps(data).encode())
    logging.info(client_socket.recv(1024).decode())


def logout():
    data = {'command': 'logout', 'data': ''}
    client_socket.send(json.dumps(data).encode())
    logging.info(client_socket.recv(1024).decode())
    data = {'command': 'disconnect'}
    client_socket.send(json.dumps(data).encode())


def get():
    data = {'command': 'get', 'data': ''}
    client_socket.send(json.dumps(data).encode())
    logging.info(json.loads(client_socket.recv(1024).decode())['msg'])


def get_contact():
    print("User's id: ")
    target_id = int(input())
    data = {'command': 'get_contact', 'data': {'key': public_key, 'target': target_id}}
    client_socket.send(json.dumps(data).encode())
    if client_socket.recv(10) != b'OK':
        logging.error(f"User with id: {target_id} not found")
        return
    client_socket.close()
    channel = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    channel.connect((host, listener_port))
    channel.listen()
    c, a = channel.accept()
    msg = client_socket.recv(1024)
    print("Received:", msg)
    # partner_key = json.loads(client_socket.recv(1024).decode())['msg']
    # print(c.recv(1024).decode())
    c.close()


def communicate():
    # if flag is False:
    #     logging.info('Did not select partner, firstly make connection')
    #     return
    #
    # if target_id > self_id:
    #     partner_socket.bind((host, 12345))
    #     partner_socket.listen()
    #     conn, a = partner_socket.accept()
    #     print(conn.recv(1024))
    #     print('dljklkfj')
    # else:
    #     partner_socket.connect((host, 12345))
    #     partner_socket.send(b"ahoi")
    #     print('laslkdj')
    #
    # partner_socket.send(json.dumps({'msq': crypto.encrypt_mh(b'Greetings', partner_key)}).encode())
    # print("Sent")
    # x = json.loads(partner_socket.recv(1024).decode())['msg']
    # print(crypto.decrypt_mh(x, private_key))
    pass


def _get_selection(prompt, options):
    choice = input(prompt).upper()
    while not choice or choice[0] not in options:
        choice = input("Please enter one of {}. {}".format('/'.join(options), prompt)).upper()
    return choice[0]


def get_yes_or_no(prompt, reprompt=None):
    if not reprompt:
        reprompt = prompt

    choice = input("{} (Y/N) ".format(prompt)).upper()
    while not choice or choice[0] not in ['Y', 'N']:
        choice = input("Please enter either 'Y' or 'N'. {} (Y/N)? ".format(reprompt)).upper()
    return choice[0] == 'Y'


def get_action():
    return _get_selection("(L)ogin, L(O)gout, (G)et clients, (M)ake contact", "LOGM")


def run_suite():
    print('-' * 34)
    action = get_action()
    commands = {
        'L': login,
        'O': logout,
        'G': get,
        'M': get_contact
    }
    if action == 'O':
        return False
    commands[action]()
    return True


class PeerToPeer(threading.Thread):
    def __init__(self, port_nr):
        super().__init__()
        self.port = port_nr

    def run(self):
        channel = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        channel.connect((host, self.port))
        channel.listen()
        c, a = channel.accept()
        print(c.recv(1024).decode())
        c.close()


def test_if_port_is_free(port_nr):
    a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result_of_check = a_socket.connect_ex((host, port_nr))
    if result_of_check == 0:
        a_socket.close()
        return False
    else:
        a_socket.close()
        return True


def main():
    global listener_port
    try:
        listener_port = int(sys.argv[1])
    except ValueError:
        print("Usage: python client.py [port_number]")
        return
    if test_if_port_is_free(listener_port) is False:
        print("Port number already in use")
        return
    try:
        client_socket.connect((host, port))
    except ConnectionRefusedError:
        print("Key server is not running")
        return
    print("Connection established with the server")
    print("Welcome to the Client Suite!")
    logging.basicConfig(level=logging.INFO)
    logging.info(f'public key: {public_key}')
    run_suite()
    while get_yes_or_no("Again?"):
        if run_suite() is False:
            break
    print("Goodbye!")
    client_socket.close()
    # client_socket = socket.socket()
    # client_socket.connect((host, port))
    # data = {'command': 'get_contact', 'data': {'target': 123}}
    # client_socket.send(json.dumps(data).encode())
    # print(client_socket.recv(125))


if __name__ == '__main__':
    main()
