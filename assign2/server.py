import json
import logging
import socket
import threading

host, port = 'localhost', 12356

clients = {}
ports = {}


def login(conn, data):
    try:
        logging.info(f"User with id: {data['id']} registered its public key: {data['key']}")
        ports[conn.getsockname()[1]] = data['id']
        clients[data['id']] = data['key']
        conn.send('OK'.encode())
    except KeyError:
        logging.error("Some of the values were not correct in the login message")


def get(conn, data):
    conn.send(json.dumps({'msg': clients}).encode())


def logout(conn, data):
    try:
        clients.pop(data['id'])
        conn.send('OK'.encode())
    except ValueError:
        conn.send('ERROR'.encode())


def get_contact(conn, data):
    try:
        # key = clients[data['id']]
        conn.send(json.dumps({'msg': 'OK'}).encode())
        client_socket_1 = socket.socket()
        client_socket_1.connect((host, ports[data['id']]))
        client_socket_1.send(b'message')
        client_socket_2 = socket.socket()
        client_socket_2.connect((host, data['target']))
        client_socket_2.send(b'messgfd')
    except KeyError:
        conn.send(json.dumps({'msg': f"Couldn't find client with id: {data['target']}"}).encode())


commands = {'login': lambda x, y: login(x, y), 'logout': lambda x, y: logout(x, y),
            'get': lambda x, y: get(x, y), 'get_contact': lambda x, y: get_contact(x, y)}


class ClientThread(threading.Thread):
    def __init__(self, conn, address):
        threading.Thread.__init__(self)
        self.address = address
        self.conn = conn

    def run(self):
        while True:
            try:
                raw = self.conn.recv(1024)
                processed = json.loads(raw)
                if processed['command'] == 'disconnect':
                    break
                commands[processed['command']](self.conn, processed['data'])
            except ConnectionError:
                break


def run_server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    logging.basicConfig(level=logging.INFO)
    logging.info("Server socket bound to port %d", port)
    server_socket.listen()
    logging.info("Server started")
    while True:
        conn, address = server_socket.accept()
        logging.info("Server received connection from %s", address)
        ClientThread(conn, address).start()


def main():
    run_server()


if __name__ == '__main__':
    main()
